# CPUnix86
Hardware monitoring/benchmarking/stress testing utility for unix x86 systems.

This was my very rushed disertation project.  I'm currently porting the UI to Qt before resuming development. Though I'm more focused on SECter atm.

What currently works:
    -Single threaded CPU benchmarking
    -The UI (to some degree)
    -Temprature monitoring (as long as the benchmark isn't running)

What doesn't work:
    -Temps while a benchmark is running.
    -Pretty much any code related to RAM or storage
  
  
To run the software as-is, run the following: 
    git clone https://www.github.com/AdamFx990/CPUnix86.git
    cd /CPUnix86
    ./project

The code is pretty horrible looking at the moment. There's a big commit in the works that'll address the messy code. I might add the Qt version to its own repo and make this version bash-only.
