# Directories
TOPDIR ?= $(CURDIR)
BINDIR = bin
SRCDIR = src

# change application name here (executable output name)
TARGET	:= cpunix86
BULID	:= build
SOURCES	:= sources
DATA	:= data
INCLUDES:= include
#------------------------------------------------------

# compiler
CC=clang
# debug
DEBUG=-g
# optimisation
OPT=-O0
# warnings
WARN=-Wall

INC_DIR=../$(SRCDIR)

PTHREAD=-lX11 -pthread

CCFLAGS=$(DEBUG) $(OPT) $(WARN) -I$(INC_DIR) $(PTHREAD) -pipe

GTKLIB=`pkg-config --cflags --libs gtk+-3.0`

# linker
LD=clang
LDFLAGS=$(PTHREAD) $(GTKLIB) -export-dynamic

OBJ_MAIN	=$(BINDIR)/main.o
OBJ_CPU		=$(BINDIR)/cpu.o
OBJ_MEM		=$(BINDIR)/mem.o
OBJ_BENCHCPU=$(BINDIR)/benchCpu.o
OBJ_STRESS	=$(BINDIR)/stress.o

OBJS=$(OBJ_MAIN) $(OBJ_CPU) $(OBJ_MEM) $(OBJ_BENCHCPU) $(OBJ_STRESS)

all: $(OBJS)
	$(LD) -o $(TARGET) $(OBJS) $(LDFLAGS)

$(OBJ_MAIN): $(SRCDIR)/main.c
	$(CC) -c $(CCFLAGS) $(SRCDIR)/main.c $(GTKLIB) -o $(BINDIR)/main.o

$(OBJ_CPU): $(SRCDIR)/cpu.c
	$(CC) -c $(SRCDIR)/cpu.c $(GTKLIB) -o $(BINDIR)/cpu.o

$(OBJ_MEM): $(SRCDIR)/mem.c
	$(CC) -c $(SRCDIR)/mem.c $(GTKLIB) -o $(BINDIR)/mem.o

$(OBJ_BENCHCPU): $(SRCDIR)/benchCpu.c
	$(CC) -c $(SRCDIR)/benchCpu.c $(GTKLIB) -o $(BINDIR)/benchCpu.o

$(OBJ_STRESS): $(SRCDIR)/stress.c
	$(CC) -c $(SRCDIR)/stress.c  $(GTKLIB) -o $(BINDIR)/stress.o

clean:
	rm -f bin/*.o $(TARGET)
