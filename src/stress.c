#include "stress.h"
//---------------------
struct thread_struct {
//---------------------
  int          thread;
  GtkBuilder   *builder; 
  GtkLabel     *lbl_thread;
  GtkLabel     *lbl_total;
  GtkLabel     *lbl_latest;
//---------------------
};
//---------------------

//---------------------
void prime(void *argStruct) {
//---------------------
	struct thread_struct *args = (struct thread_struct *)argStruct;
  int i = 3, count, c;
  char buffer[16];
	const int n = INT_MAX;
  //---------------------
 
   if ( n >= 1 ) {
    	printf("First %d prime numbers are :\n",n);
   }
 
   for ( count = 2 ; count <= n ;  ) {
      for ( c = 2 ; c <= i - 1 ; c++ ) {
         if ( i%c == 0 )
            break;
      }
      if ( c == i ) {
        sprintf(buffer, "%i", i); 
      	printf("Thread %i: %s\n", (*args).thread, buffer);
        gtk_label_set_text((*args).lbl_total, "buffer");
        
        count++;
      }
      i++;
   }
//---------------------
}
//---------------------

//------------------------------
void debugMesg(void *argStruct) {
//------------------------------
  struct thread_struct *args = (struct thread_struct *)argStruct;
	printf("A thread with ID: %i was created\n", (*args).thread);	
	prime(args);
//---------------------
}
//---------------------

//------------------------------
void genThread(GtkBuilder *builder, int thread) {
//------------------------------
	XInitThreads();
	GtkLabel        *lbl_thread;
  GtkLabel        *lbl_total;
  GtkLabel        *lbl_latest;
  //---------------------
	int        proc;
	pthread_t  prime; 
	char       id[8];
  struct thread_struct *args;
  //---------------------
  lbl_thread = GTK_LABEL(gtk_builder_get_object(builder, "lbl_thread"));
  lbl_total  = GTK_LABEL(gtk_builder_get_object(builder, "lbl_total"));
  lbl_latest = GTK_LABEL(gtk_builder_get_object(builder, "lbl_latest"));
	sprintf(id, "%i", thread);
	gtk_label_set_text(lbl_thread, id);
  //---------------------
  proc = malloc(thread*sizeof(pthread_t));
  args = malloc(sizeof(struct thread_struct));
  /*WHY ALL THE MALLOCS AND POINTERS?
  genThread() is being run in a loop whenever the stress button is pressed.
  The thread_struct is being created each time with the same memory allocated
  UNLESS it is manually given a fresh place to write the values. Hence the
  funky looking code here (at least I think that's what's happening) If you
  can make this better, be my guest. */
  printf("creating struct: %i\n", thread);
  (*args).thread     = thread;
  (*args).builder    = builder;
  (*args).lbl_thread = lbl_thread;
  (*args).lbl_total  = lbl_total;
  (*args).lbl_latest = lbl_latest;
  //---------------------

	proc = pthread_create(&prime, NULL, debugMesg, (void *) args);
//---------------------
}
//---------------------

//------------------------------
void cancelThreads(int thread) {
//------------------------------
	printf("cancelThreads");
}