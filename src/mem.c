//Header import(s)---------------
#include "mem.h"
//Global variables---------------

//-------------------------------
int memReadFile(char *search) {
//-------------------------------
  FILE* fp;
  char buffer[1000000];
  size_t bytes_read;
  char* match;
  int result;
  char* string = " : %i";
  char* fullString = malloc(strlen(search)+16);
//---Read the entire contents of /proc/cpuinfo into the buffer--
  fp = fopen ("/proc/meminfo", "r");
  bytes_read = fread (buffer, 1, sizeof (buffer), fp);
  fclose (fp);
  /* Bail if read failed or if buffer isn't big enough.  */
  if (bytes_read == 0 || bytes_read == sizeof (buffer))
    return 1;
  /* NUL-terminate the text.  */
  buffer[bytes_read] = '\0';
  /* Locate the line that starts with "cpu MHz".  */
  match = strstr (buffer, search);
  if (match == NULL)
    return 2;
  /* Parse the line to extrace the clock speed.  */
  strcpy(fullString, search);
  strcat(fullString, string);

  sscanf (match, fullString, &result);
  return result;
}
//-------------------------------
double memTotal () {
//-------------------------------
  char search[8] = "MemTotal";  //Specify what to search the file for
  return memReadFile(search)/1024.00;
}
//-------------------------------
double memFree () {
//-------------------------------
  char search[7] = "MemFree";
  return memReadFile(search)/1024.00;
}
//-------------------------------
double memUsed () {
//-------------------------------
  double mem = memReadFile("MemTotal");
  mem = mem - memReadFile("MemFree");
  return mem/1024.00;
}
