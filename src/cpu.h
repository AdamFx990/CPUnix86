//system libraries---------------
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>
//non-system libraries-----------

//Global variables---------------
int maxTempVar;
int minTempVar;
//-------------------------------

//function declerations----------
int cpuReadFile(char *search);
int siblings();
int mhz();
int cores();
int temp();
int maxTemp();
int minTemp();

