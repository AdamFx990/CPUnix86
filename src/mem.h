//system libraries---------------
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>
//non-system libraries-----------

//Global variables---------------

//-------------------------------

//function declerations----------
int memReadFile(char *search);
double memTotal(); 
double memFree();
double memUsed();
