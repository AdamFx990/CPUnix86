//system libraries---------------
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
//non-system libraries-----------
#include "cpu.h"
#include "mem.h"
#include "benchCpu.h"
#include "stress.h"

//Global variables---------------
GtkBuilder *builder;
//-------------------------------

//-------------------------------
char* model () {
//-------------------------------
    FILE* fp;
    size_t bytes_read;
    char* match;
    char* result;
    char buffer[10000];
    /* Read the entire contents of /proc/cpuinfo into the buffer.  */
    fp = fopen ("/proc/cpuinfo", "r");
    bytes_read = fread (buffer, 1, sizeof (buffer), fp);
    fclose (fp);
    /* Bail if read failed or if buffer isn't big enough.  */
    if (bytes_read == 0 || bytes_read == sizeof (buffer))
    return "no bytes";
    /* NUL-terminate the text.  */
    buffer[bytes_read] = '\0';
    /* Locate the line that starts with "cpu MHz".  */
    match = strstr (buffer, "model name");
    if (match == NULL)
    return "";

    sscanf (match, "model name : %s ", &result);

    printf("%s\n", result);

    return result;
//-------------------------------
}
//-------------------------------

//-------------------------------
void on_btn_stress_clicked () {
//-------------------------------
    int threads = siblings();
    //----1 BUILDER PER THREAD------------
    GtkWidget       *winStress;
    GtkBuilder      *sBuilder[threads];
    //----1 WINDOW PER PHYSICAL THREAD----
    printf("Generating threads...\n");
    for(int i = 0; i < threads; i++) {
        //------------------------------------
        sBuilder[i] = gtk_builder_new();
        gtk_builder_add_from_file (sBuilder[i], "glade/window_main.glade", NULL);
        winStress = GTK_WIDGET(gtk_builder_get_object(sBuilder[i], "win_thread"));
        gtk_widget_show(winStress);
        gtk_widget_queue_draw(winStress);

        genThread(sBuilder[i], i);
    }
//-------------------------------
}
//-------------------------------

//---------------------------------
void on_btn_benchmarkStart_clicked() {
    //---------------------------------
    GtkEntry *txt_iterations;
    GtkEntry *txt_maxPrime;
    GtkLabel *lbl_result;

    char  *buffer;
    txt_iterations = GTK_ENTRY(gtk_builder_get_object(builder, "txt_iterations"));
    txt_maxPrime   = GTK_ENTRY(gtk_builder_get_object(builder, "txt_maxPrime"  ));
    lbl_result     = GTK_LABEL(gtk_builder_get_object(builder, "lbl_result"    ));

    buffer = gtk_entry_get_text(txt_iterations);
    int lim = atoi(buffer);
    printf("lim: %i\n", lim);

    buffer = gtk_entry_get_text(txt_maxPrime);
    int i = atoi(buffer);
    printf("i: %i\n", i);
    if(lim == 0 || i == 0){
        gtk_entry_set_text(txt_iterations, "Try again");
    }
    else {
        int msec = setup(lim, i);
        printf("Time taken %d seconds %d milliseconds\n", msec / 1000, msec % 1000);
        char buffer[64];
        sprintf(buffer, "Time taken %d seconds %d milliseconds", msec / 1000, msec % 1000);
        gtk_label_set_text(lbl_result, buffer);
    }
//---------------------------------
}
//---------------------------------

//---------------------------------
void on_btn_benchmark_clicked () {
//---------------------------------
    GtkWidget       *winBenchmark;
    //-------------------------------
    GtkEntry        *txt_iterations;
    GtkEntry        *txt_maxPrime;
    //-------------------------------
    const int rec = 10000;
    winBenchmark = GTK_WIDGET(gtk_builder_get_object(builder, "win_benchmark"));
    gtk_widget_show(winBenchmark);
    gtk_widget_queue_draw(winBenchmark);
    //-------------------------------
    txt_iterations = GTK_ENTRY(gtk_builder_get_object(builder, "txt_iterations"));
    txt_maxPrime = GTK_ENTRY(gtk_builder_get_object(builder, "txt_maxPrime"));
    //-------------------------------
    gtk_entry_set_text(txt_iterations, "10000");  //recommended value
    gtk_entry_set_text(txt_maxPrime  , "10000");
//-------------------------------
}
//-------------------------------

//----UPDATES THE LABELS---------
bool updateFreq(gpointer data) {
//----------LABELS---------------
    GtkLabel        *lbl_clock;
    GtkLabel        *lbl_core;
    GtkLabel        *lbl_thread;
    GtkLabel        *lbl_temp;
    GtkLabel        *lbl_minTemp;
    GtkLabel        *lbl_maxTemp;
    GtkLabel        *lbl_cpuId;

    GtkLabel        *lbl_memTotal;
    GtkLabel        *lbl_memFree;
    GtkLabel        *lbl_memUsed;
    //---------BUTTONS-------------
    GtkButton       *btn_stress;
    GtkButton       *btn_benchmark;
    // LABELS
    lbl_clock    = GTK_LABEL(gtk_builder_get_object(builder, "lbl_clock_right"  ));
    lbl_core     = GTK_LABEL(gtk_builder_get_object(builder, "lbl_core_right"   ));
    lbl_thread   = GTK_LABEL(gtk_builder_get_object(builder, "lbl_thread_right" ));
    lbl_temp     = GTK_LABEL(gtk_builder_get_object(builder, "lbl_curTemp_right"));
    lbl_minTemp  = GTK_LABEL(gtk_builder_get_object(builder, "lbl_minTemp_right"));
    lbl_maxTemp  = GTK_LABEL(gtk_builder_get_object(builder, "lbl_maxTemp_right"));
    lbl_cpuId    = GTK_LABEL(gtk_builder_get_object(builder, "lbl_cpu_id"       ));
    lbl_memTotal = GTK_LABEL(gtk_builder_get_object(builder, "lbl_memTotal"     ));
    lbl_memFree  = GTK_LABEL(gtk_builder_get_object(builder, "lbl_memFree"      ));
    lbl_memUsed  = GTK_LABEL(gtk_builder_get_object(builder, "lbl_memUsed"      ));
    // BUTTONS
    btn_stress    = GTK_BUTTON(gtk_builder_get_object(builder, "btn_stress"  ));
    btn_benchmark = GTK_BUTTON(gtk_builder_get_object(builder, "btn_benchmark"));

    char buffer[512];
    //-----UPDATE CPU TAB LABELS-----------------------------------------------------
    sprintf(buffer, "%i MHz", mhz());
    gtk_label_set_text(lbl_clock, buffer);  // Change the label to display core clock speed
    sprintf(buffer, "%i", cores());
    gtk_label_set_text(lbl_core, buffer);
    sprintf(buffer, "%i", siblings());
    gtk_label_set_text(lbl_thread, buffer); // Update the core_right label to show the current number of siblings
    sprintf(buffer, "%i°C", temp());
    gtk_label_set_text(lbl_temp, buffer);
    sprintf(buffer, "%i°C", minTemp());
    gtk_label_set_text(lbl_minTemp, buffer);
    sprintf(buffer, "%i°C", maxTemp());
    gtk_label_set_text(lbl_maxTemp, buffer);
    //sprintf(buffer, "%s", model());
    //gtk_label_set_text(lbl_cpuId, buffer);
    //-----UPDATE RAM TAB LABELS---------------
    sprintf(buffer, "%.2f  MB", memTotal());
    gtk_label_set_text(lbl_memTotal, buffer);
    sprintf(buffer, "%.2f  MB", memFree());
    gtk_label_set_text(lbl_memFree, buffer);
    sprintf(buffer, "%.2f  MB", memUsed());
    gtk_label_set_text(lbl_memUsed, buffer);

    return true;
//-------------------------------
}
//-------------------------------

//---------------------------------
int main(int argc, char *argv[]) {
//---------------------------------
    GtkWidget       *winMain;
    //---------------------------------------------------------------------
    gtk_init(&argc, &argv);
    builder = gtk_builder_new();
    gtk_builder_add_from_file (builder, "glade/window_main.glade", NULL);
    winMain = GTK_WIDGET(gtk_builder_get_object(builder, "win_menu"));

    gtk_builder_connect_signals(builder, NULL);
    //---------------------------------------------------------------------
    gint func_ref = g_timeout_add(200, updateFreq, NULL); //Set the frequency of UI updates (in milliseconds)
    gtk_widget_queue_draw(winMain); //Initial UI update (only triggered once!)
    //---------------------------------------------------------------------
    gtk_widget_show(winMain);
    gtk_main(); //Handles the subsequent updates as per g_timeout_add frequency
    g_source_remove (func_ref);
    //---------------------------------------------------------------------
    return 0;
//-------------------------------
}
//-------------------------------

//------------------------------
void on_window_main_destroy() { // called when window is closed
//------------------------------
    gtk_main_quit();
//------------------------------
}
//------------------------------
