//Header import(s)---------------
#include "benchCpu.h"
//Global variables---------------
#define LIMIT 1500000 /*size of integers array*/
#define PRIMES 100000 /*size of primes array*/
//-------------------------------

//---------------------
struct bench_struct {
//---------------------
  int lim, i;
//---------------------
};
//---------------------

//--------------------
int benchCpu(void* parStruct) {
//--------------------


	struct bench_struct *par = (struct bench_struct *)parStruct;
	// Create a boolean array "prime[0..n]" and initialize
    // all entries it as true. A value in prime[i] will
    // finally be false if i is Not a prime, else true.
    clock_t start = clock(), diff;
    for(int j = 0; j < (*par).lim; j++) {
	    bool prime[(*par).i+1];
	    memset(prime, true, sizeof(prime));

	    for (int p=2; p*p<=(*par).i; p++)
	    {
	        // If prime[p] is not changed, then it is a prime
	        if (prime[p] == true)
	        {
	            // Update all multiples of p
	            for (int i=p*2; i<=(*par).i; i += p)
	                prime[i] = false;
	        }
	    }

	    // Print all prime numbers
	    for (int p=2; p<=(*par).i; p++)
	       if (prime[p])
	          printf("%i\n", p);
	}
	diff = clock() - start;
	const int msec = diff * 1000 / CLOCKS_PER_SEC;
	return msec;

//--------------------
}
//--------------------

//--------------------
int setup(int lim, int i) {
//--------------------
	int *proc;
	pthread_t sieve;

	struct bench_struct *par;

	(*par).i = i;
	(*par).lim = lim + 1;

	pthread_create(&sieve, NULL, benchCpu, (void*) par);
	pthread_join(sieve, &proc);
	return *proc;



//--------------------
}
//--------------------
