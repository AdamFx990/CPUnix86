#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <limits.h>
#include <gtk/gtk.h>

void prime(void *argStruct);
void debugMesg(void *argStruct);
void genThread(GtkBuilder *builder, int thread);
void cancelThread(int thread);