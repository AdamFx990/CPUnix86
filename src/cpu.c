//Header import(s)---------------
#include "cpu.h"
//Global variables---------------
maxTempVar = -9999;
minTempVar = -9999;
//-------------------------------
int cpuReadFile(char *search) {
//-------------------------------
  FILE* fp;
  char buffer[1000000];
  size_t bytes_read;
  char* match;
  int result;
  char* string = " : %i";
  char* fullString = malloc(strlen(search)+16);
//---Read the entire contents of /proc/cpuinfo into the buffer--
  fp = fopen ("/proc/cpuinfo", "r");
  bytes_read = fread (buffer, 1, sizeof (buffer), fp);
  fclose (fp);
  /* Bail if read failed or if buffer isn't big enough.  */
  if (bytes_read == 0 || bytes_read == sizeof (buffer))
    return 1;
  /* NUL-terminate the text.  */
  buffer[bytes_read] = '\0';
  /* Locate the line that starts with "cpu MHz".  */
  match = strstr (buffer, search);
  if (match == NULL)
    return 2;
  /* Parse the line to extrace the clock speed.  */
  strcpy(fullString, search);
  strcat(fullString, string);

  sscanf (match, fullString, &result);
  return result;
}
//-------------------------------
int siblings () {
//-------------------------------
  char search[8] = "siblings";  //Specify what to search the file for
  return cpuReadFile(search);  //Search the file
}
//-------------------------------
int mhz () {
//-------------------------------
  char search[7] = "cpu MHz";
  return cpuReadFile(search);
}
//-------------------------------
int cores () {
//-------------------------------
  char search[9] = "cpu cores"; //Having the array size sent incorrectly causes weird values to be returned.
  return cpuReadFile(search);
}
//-------------------------------
int temp () {
//-------------------------------
  FILE* fp;
  char buffer[8];
  size_t bytes_read;
  char* match;
  int celcius;

  /* Read the entire contents of the file into the buffer.  */
  fp = fopen ("/sys/class/thermal/thermal_zone0/temp", "r");
  bytes_read = fread (buffer, 1, sizeof (buffer), fp);
  fclose (fp);
  /* Bail if read failed or if buffer isn't big enough.  */
  if (bytes_read == 0 || bytes_read == sizeof (buffer))
    return 1;
  /* NUL-terminate the text.  */
  buffer[bytes_read] = '\0';

  sscanf (buffer, "%i", &celcius);
  celcius = celcius / 1000; //Removes redundent naughts from int

  return celcius;
}
//-------------------------------
int minTemp() {
//-------------------------------
  int cur = temp();
  if(minTempVar == -9999) minTempVar = cur;  //if no mintemp has been set, make it the current tempreature.
  if(cur < minTempVar) minTempVar = cur;  //if current temp is less than minTemp, current tempreature.
  return minTempVar;
}
//-------------------------------
int maxTemp() {
//-------------------------------
  int cur = temp();
  if(cur > maxTempVar) maxTempVar = cur;  //if current temp is higher than maxTemp, set current temp as max.
  return maxTempVar;
}

